This is the rabbitMQ impkementation for the prototype.

ASSUMES you have pika package and rabbitMQ server installed.
installation-
    For pika - pip install pika
    For rabbitmq - apt-get install rabbitmq-server

USAGE
=====
This implementation has two types of scripts, send.py and recv(12/2).py

send.py is the implementation for the sending side, i.e. the mailman server in our case
send.py sends a message which is basically timestamp + mailinglistname(use mlist1/mlist2 for now). The mailing list name is to be given as a command line argument to send.py.
Run send.py as - 
python send.py mlist1
OR
python send.py mlist2

recv(12/2).py simulates the archive side of the implementation.
recv12.py archives mails from both mlist1 and mlist2.
recv2.py archives mails only from mlist2.
Run recv(12/2).py as - 
python recv12.py
OR
python recv2.py
message is printed just as it is received by the receiver.


