import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

client  = 'client2'

channel.exchange_declare(exchange='logs',type='direct')
result = channel.queue_declare(queue=client)
queue_name = result.method.queue
print(queue_name)
channel.queue_bind(exchange='logs',queue=queue_name,routing_key='mlist2')

def callback(ch, method, properties, body):
    print('received %s' %body)

channel.basic_consume(callback,
                      queue=queue_name,
                      no_ack=True)

channel.start_consuming()
