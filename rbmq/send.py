import pika
import sys
from datetime import datetime
print sys.argv
connection = pika.BlockingConnection(pika.ConnectionParameters(host="localhost"))

channel = connection.channel()
msg = str(datetime.now()) + ' ' + sys.argv[1]
channel.exchange_declare(exchange='logs', type='direct')
channel.basic_publish(exchange='logs',
                      routing_key=sys.argv[1],
                      body=msg)

print(msg)
connection.close()
